var functionsObject;

if (typeof(Android) !== 'undefined') {
    functionsObject = {
        log: function (e) {
            Android.logEvent(e);
        },
        minimize: function () {
            Android.minimizeChat();
        },
        maximize: function () {
            Android.maximizeChat();
        },
        close: function () {
            Android.closeChat();
        }
    };
}
else{
    console.log('No Android JavaScript interface detected')
}


   
