function leEventsBinding() {

    console.log("binding script");

    lpTag.events.bind('LP_OFFERS', 'OFFER_IMPRESSION', function (cmdData) {
        console.log('Offer Impression ' + JSON.stringify(cmdData));
        functionsObject.log('Offer Impression ' + JSON.stringify(cmdData));
        if (cmdData.state === 1) {
            setTimeout(function () {
                lpTag.taglets.rendererStub.click(cmdData.engagementId.toString());
            }, 1000);

        }
    });

    lpTag.events.bind('lpUnifiedWindow', 'minimized', function () {
          functionsObject.minimize();




var ctrlBtns = document.getElementsByClassName('lp_header-buttons-container')[0]

        var minText = document.getElementsByClassName('lp_title')[0];
        var arrow = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%" viewBox="0 0 110 120" xml:space="preserve"><polyline fill="white" stroke="#FFFFFF" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="140,90 50,10 -30,90  140,90"/></svg>'
        minText.setAttribute("style", "color:#FFFFFF;font-style:normal;font-family:Arial,Helvetica,sans-serif;font-weight:normal;width: 100vh;height: 100vw;max-width: 2000px;position: absolute;top: 90px;left: -90px;right: 0; font-size: 75pt;text-align: center;");
            minText.innerHTML = arrow;
           ctrlBtns.style.display = 'none';

    });

    lpTag.events.bind('lpUnifiedWindow', 'maximized', function () {
        functionsObject.maximize();
        document.getElementsByClassName('lp_top-text')[0].cssText = "";

    });

    lpTag.events.bind('lpUnifiedWindow', 'windowClosed', function () {
       functionsObject.close();
    });
}