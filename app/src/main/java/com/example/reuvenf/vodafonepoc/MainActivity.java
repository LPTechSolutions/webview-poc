package com.example.reuvenf.vodafonepoc;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        HomeFragment homeFragment = new HomeFragment();

        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragmentContainer, homeFragment).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.startChat) {
            ChatFragment chatFragment = new ChatFragment();

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragmentContainer, chatFragment).addToBackStack(null).commit();


        }

        return super.onOptionsItemSelected(item);
    }

    void maximizeChatWindow() {
//        Fragment chatFragment = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
//        View contentView = getWindow().findViewById(Window.ID_ANDROID_CONTENT);
//
//        int windowHeight = contentView.getBottom();
//        int windowWidth = contentView.getRight();
//        ViewGroup.LayoutParams params = chatFragment.getView().getLayoutParams();
//        params.height = windowHeight;
//        params.width = windowWidth;
//        chatFragment.getView().setLayoutParams(params);

    }

    void minimizeChatWindow() {
//        FrameLayout fragmentContainer = (FrameLayout) findViewById(R.id.fragmentContainer);
//        fragmentContainer.getLayoutParams().height = 460;
//        fragmentContainer.getLayoutParams().width = 350;
//        fragmentContainer.requestLayout();

    }

    void closeChatWindow() {
        Fragment chatFragment = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
        getSupportFragmentManager().beginTransaction().remove(chatFragment).commit();
    }

}
