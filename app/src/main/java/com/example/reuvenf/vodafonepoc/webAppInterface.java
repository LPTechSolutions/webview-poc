package com.example.reuvenf.vodafonepoc;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.webkit.JavascriptInterface;

class WebAppInterface {

    Activity mContext;

    WebAppInterface(Activity c) {
        mContext = c;
    }

    @JavascriptInterface
    public void logEvent(String logString) {
        Log.d("LP_EVENTS", logString);
    }

    @JavascriptInterface
    public void minimizeChat() {
        Log.d("LP_WINDOW", "Minimized");
        MainActivity mainActivity = (MainActivity) mContext;
        mainActivity.minimizeChatWindow();
    }

    @JavascriptInterface
    public void maximizeChat() {
        Log.d("LP_WINDOW", "Maximized");
        MainActivity mainActivity = (MainActivity) mContext;
        mainActivity.maximizeChatWindow();
    }

    @JavascriptInterface
    public void closeChat() {
        Log.d("LP_WINDOW", "Closed");
        MainActivity mainActivity = (MainActivity) mContext;
        mainActivity.closeChatWindow();
    }
}