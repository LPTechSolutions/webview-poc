package com.example.reuvenf.vodafonepoc;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class ChatFragment extends Fragment {


    public ChatFragment() {
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_chat, container, false);
    }


    @Override
    public void onActivityCreated (Bundle savedInstanceState){
        startChat();
        super.onActivityCreated(savedInstanceState);
    }



    void startChat(){

        WebView webView = (WebView) getActivity().findViewById(R.id.chatWebView);

        WebSettings webSettings = webView.getSettings();


        webView.setWebViewClient(new WebViewClient());
            webView.loadUrl("http://10.0.2.2:8888/android_apps/VodafonePOC/app/src/main/res/raw/index.html?platform=android&section=sales");


        webSettings.setJavaScriptEnabled(true);
       // webSettings.setUserAgentString("lp-user");
        webSettings.setDomStorageEnabled(true);

        webView.addJavascriptInterface(new WebAppInterface(getActivity()), "Android");
    }
}
